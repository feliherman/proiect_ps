package com.example.springboot.controller;

import com.example.springboot.connectionPackage.DataBase;
import com.example.springboot.dbHelper.Team;
//import com.example.springboot.data.TeamQuery;
import com.example.springboot.dbHelper.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.management.Query;
import java.util.ArrayList;

/**
 * clasa care interactioneaza cu baza de date
 */
@RestController
public class HelloController {

    private static final String template = "%s";

    @Autowired
    //private TeamQuery teamQuery;

    /**
     * test get
     * @return
     */
    @GetMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    /**
     * ia un user cu username si parola din baza de date
     */
    @GetMapping("/getter")
    public com.example.springboot.dbHelper.User getUser(@RequestParam(value = "user", defaultValue = "username") String user, @RequestParam(value = "password", defaultValue = "0000") String password) {

        return new com.example.springboot.dbHelper.User(0, String.format(template, user), String.format(template, password), 0);
    }

    /**
     * selecteaza prima echipa din tabela teams
     * @return team
     */
    @GetMapping("/firstTeam")
    public Team getTeam() {

        DataBase db = new DataBase();
        Team t = db.getFirstTeam();
        return t;
        /*Iterable<Team> result = teamQuery.findAll();
        Team t = new Team();
        ArrayList<Team> allTeams = new ArrayList<>();
        result.forEach(allTeams::add);
        return allTeams.get(0);*/

    }

    /**
     * adauga in baza de date un nou user
     * @param username
     * @param password
     * @return
     */
    @PostMapping("/add")
    public String addUser(@RequestParam String username, @RequestParam String password) {
        User newUser = new User();
        newUser.setUsername(username);
        newUser.setPassword(password);
        //teamQuery.addUser(newUser);
        return "User added to database!";
    }

    /**
     * sterge din tabela teams echipa cu id ul dat ca parametru
     * @param id
     */
    @DeleteMapping("/teams/{id}")
    public void deleteTeam(@PathVariable int id) {
        //teamQuery.deleteById(id);
    }
}
