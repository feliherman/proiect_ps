package com.example.springboot.connectionPackage;

import com.example.springboot.dbHelper.Team;

import java.sql.*;

/**
 * Am incercat sa fac o clasa pentru a folosi query, insa nu am reusit
 */
public class DataBase {
    private final static String getAllTeamsQuery = "SELECT * from teams";

    public DataBase() {
    }

    /**
     * @return all the teams from the table teams
     * com.example.springboot.data will be stored in ResulSet
     */
    public Team getFirstTeam() {

        PreparedStatement getStatement = null;
        Connection conn;
        ConnectionFactory con = new ConnectionFactory();
        conn=con.createConnetion();
        System.out.println("CONEXIUNEA E FACUTA" + conn);
        ResultSet result = null;
        try {
            getStatement = conn.prepareStatement(getAllTeamsQuery, Statement.RETURN_GENERATED_KEYS);
            getStatement.execute();
            result = getStatement.getResultSet();
            result.first();
            Team t = new Team(result.getInt(1),result.getString(2),result.getInt(3), result.getInt(4),result.getInt(5),
                            result.getInt(6),result.getInt(7),result.getInt(8),result.getInt(9));
            return t;
            //System.out.println(result.getString(2));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
