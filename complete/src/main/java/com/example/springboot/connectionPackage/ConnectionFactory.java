package com.example.springboot.connectionPackage;

import java.sql.*;
import java.util.logging.Logger;
import java.util.Properties;
/**
 * In aceasta clasa am realizat conexiunea la baza de date
 * */
public class ConnectionFactory {

    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER ="com.mysql.cj.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/mydatabase";
    private static final String USER = "root";
    private static final String PASS = "nicusorstanciu99";
    private static Connection con;


    public ConnectionFactory() {
        try{
            Class.forName(DRIVER);
        }
        catch(ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    public Connection createConnetion(){
        try {
            con=DriverManager.getConnection(DBURL,USER, PASS);
            if (con != null) {
                System.out.println("Database Connected successfully");
            } else {
                System.out.println("Database Connection failed");
            }
            return(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     *
     * @return conexiunea
     */
    public Connection getConnection(){
        try {

            Class.forName(DRIVER);

            con= DriverManager.getConnection(DBURL,USER,PASS);

            System.out.println("Conectat la baza de date!");
            return con;

        }
        catch(Exception e){

            System.out.println("NU S A CONECTAT!");
            return null;
        }
    }

}
