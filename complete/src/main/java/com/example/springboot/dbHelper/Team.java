package com.example.springboot.dbHelper;

//import javax.persistence.*;

/**
 * tabela pentru echipe
 */
/*@Entity
@Table(name = "teams")*/
public class Team {
    /*@Column(name="ID")
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)*/
    private int id = 0;

    //@Column(name="Name")
    private String name;

    //@Column(name = "GamesPlayed")
    private Integer gamesPlayed;

    //@Column(name="GoalsScored")
    private Integer goalsScored;

    //@Column(name="GoalsTaken")
    private Integer goalsTaken;

    //@Column(name="Wins")
    private Integer wins;

    //@Column(name="Draws")
    private Integer draws;

    //@Column(name="Losses")
    private Integer loses;

    //@Column(name="Points")
    private Integer points;

    public Team(){

    }

    public Team(Integer id, String name,Integer gp, Integer gs, Integer gt, Integer wins, Integer draws, Integer losses, Integer points){
        this.id = id;
        this.name = name;
        this.gamesPlayed = gp;
        this.goalsScored = gs;
        this.goalsTaken = gt;
        this.wins = wins;
        this.draws = draws;
        this.loses = losses;
        this.points = points;

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGoalsScored() {
        return goalsScored;
    }

    public void setGoalsScored(Integer goalsScored) {
        this.goalsScored = goalsScored;
    }

    public Integer getGoalsTaken() {
        return goalsTaken;
    }

    public void setGoalsTaken(Integer goalsTaken) {
        this.goalsTaken = goalsTaken;
    }

    public Integer getWins() {
        return wins;
    }

    public void setWins(Integer wins) {
        this.wins = wins;
    }

    public Integer getDraws() {
        return draws;
    }

    public void setDraws(Integer draws) {
        this.draws = draws;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(Integer gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }
}
