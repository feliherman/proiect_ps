package com.example.springboot.dbHelper;

//import javax.persistence.*;

/**
 * tabela pentru useri
 */
/*@Entity
@Table(name="users")*/
public class User {
    /*@Column(name = "ID")
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)*/
    private int id = 0;

    //@Column(name = "username")
    private String username;

    //@Column(name = "password")
    private String password;

    //@Column(name = "ID_Team")
    private Integer idTeam;


    public User(){

    }

    public User(Integer id, String username, String password, Integer idTeam){
        this.id = id;
        this.username = username;
        this.password = password;
        this.idTeam = idTeam;

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(Integer idTeam) {
        this.idTeam = idTeam;
    }
}
